import rsa
import foo
from crypto import *

#Инициализация ключей
keys = foo.load_keys ('a.txt', 'b.txt')
publickey = keys[0]
privatekey = keys[1]

#грузим список нод	
node_list = []
node_list = foo.load_node_list('node_list.txt', node_list)

#исходные данные сообщения
	#адресат:
key_addr = b'-----BEGIN RSA PUBLIC KEY-----\nMIGJAoGBAIXFFV1V+9QqA+thMFR9wlh1P9acAz2Tqj0FIDsRLYO8RowhbBaGRNZk\n6po4NAdxo4/C1QlbFF0j96iEK6Hx03sz7RVtDB5x/DDpSHbGZ50vkJHdeucvmSIb\nhN1JDkGdTFT60OWu5DAcy9L1B7xCoq0zBBDX96LJP1zkMhec4zyrAgMBAAE=\n-----END RSA PUBLIC KEY-----\n'
	#полезная нагрузка:
txt = b'watsup_guys!!!'

#приступаем к шифрованию:
	
#нулевой уровень:
	
AES_key_0 = buildblock(16) #генерируем симметричный ключ нулевого уровня
	
AES_0 = AESCipher(AES_key_0)	# инициализируем объект

encrypted_txt =  AES_0.encrypt(txt) #шифруем

AES_0_with_RSA_0 = rsa.encrypt(bytes(AES_key_0, 'utf-8'), rsa.PublicKey.load_pkcs1(key_addr, 'PEM')) #шифруем AES_key_0 pub ключом адресата

level_0 = AES_0_with_RSA_0 + bytes(encrypted_txt, 'utf-8') # теперь нулевой уровень доступен только адресату



#первый уровень:

AES_key_1 = buildblock(16) #генерируем симметричный ключ 1-го уровня
	
AES_1 = AESCipher(AES_key_1)	# инициализируем объект

destination_point =  AES_1.encrypt(str(key_addr)) #шифруем пункт назначения

addr = foo.nearest(node_list, key_addr) #ищем 'ближайшего' кандидата в списке
addr_pub = b''
for i in node_list[1:]
	if i[0] = addr:
		addr_pub = i[1]

AES_1_with_RSA_1 = rsa.encrypt(AES_key_1, rsa.PublicKey.load_pkcs1(addr_pub, 'PEM')) #шифруем AES_key_1 pub ключом выбранной ноды		

package = b'0' + AES_1_with_RSA_1 + bytes(destination_point, 'utf-8') + level_0

#отправка сообщения
foo.sender(addr, package)	
